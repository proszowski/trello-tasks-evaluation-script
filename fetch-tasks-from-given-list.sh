#!/bin/bash

listName=$1

if [ -z $listName ]
then
	echo "You need to specify list name!"
	exit 13
fi

oauth=${TRELLO_OAUTH_KEY}
key=${TRELLO_API_KEY}

listId=$(. get-list-id-by-name.sh $listName)

response=$(curl --silent --request GET --url "https://api.trello.com/1/lists/$listId/cards?fields=name&members=true&member_fields=fullName&key=$key&token=$oauth")

echo $response
