#!/bin/bash

response=$(. fetch-tasks-from-given-list.sh READY-TO-VERIFICATION)

titles=$(jq '.[] | select(.name) | .name' <<< $response)

while IFS= read -ra ADDR; do
     for title in "${ADDR[@]}"; do
			members=$(jq ".[] | select(.name | contains($title)) | .members[].fullName" <<<$response)
			amountIndicator=$(echo $members | grep -o "\"" | wc -l)
		   numberOfUsers=$(($amountIndicator/2))
			if [[ $numberOfUsers -gt 0 ]]; then
				percentage=$((100/$numberOfUsers))
				if [[ $percentage -eq 100 ]]; then
					value="100%"
				else
					value="każdy po $percentage%"
				fi
			else
				value="W zadaniu uczestniczył każdy członek zespołu"
			fi
			echo $title - $members $value | tr -d "\""
     done
done <<< "$titles"
